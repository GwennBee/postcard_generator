
# Sentiment to SSML tag mapping
SENT_TAGS = {
    "joie":         ("<usel genre='calm'>    <sig amplitude='1.2' f0='+8'  rate='1.2'>", "</sig></usel>"),
    "ennui":        ("<usel genre='calm'>    <sig amplitude='1.0' f0='-10' rate='0.9'>", "</sig></usel>"),
    "colère":       ("<usel genre='stressed'><sig amplitude='1.0' f0='-10' rate='1.2'>", "</sig></usel>"),
    "tristesse":    ("<usel genre='stressed'><sig amplitude='1.0' f0='-8'  rate='1.0'>", "</sig></usel>"),
    "neutre":       ("", ""),
}

# HuggingFace API key
HF_TOKEN = "hf_xxxxxxxxxxxxxxxxxxxxxxxxxxx"

# Batch output directory
OUTPUT_DIR = "output"
